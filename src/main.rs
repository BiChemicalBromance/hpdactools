use std::fs::File;
use std::io::{Read, Write};
use clap::{App, Arg};
use plotters::prelude::*;
use regex::Regex;

#[derive(Debug)]
enum HPDRRError {}

fn plot() -> Result<(), Box<dyn std::error::Error>> {
    let root = BitMapBackend::new("plotters-doc-data/5.png", (640, 480)).into_drawing_area();
    root.fill(&WHITE);
    let root = root.margin(10, 10, 10, 10);
    // After this point, we should be able to draw construct a chart context
    let mut chart = ChartBuilder::on(&root)
        // Set the caption of the chart
        .caption("This is our first plot", ("monospace", 40).into_font())
        // Set the size of the label region
        .x_label_area_size(20)
        .y_label_area_size(40)
        // Finally attach a coordinate on the drawing area and make a chart context
        .build_cartesian_2d(0f32..10f32, 0f32..10f32)?;

    // Then we can draw a mesh
    chart
        .configure_mesh()
        // We can customize the maximum number of labels allowed for each axis
        .x_labels(5)
        .y_labels(5)
        // We can also change the format of the label text
        .y_label_formatter(&|x| format!("{:.3}", x))
        .draw()?;

    // And we can draw something in the drawing area
    chart.draw_series(LineSeries::new(
        vec![(0.0, 0.0), (5.0, 5.0), (8.0, 7.0)],
        &RED,
    ))?;
    // Similarly, we can draw point series
    chart.draw_series(PointSeries::of_element(
        vec![(0.0, 0.0), (5.0, 5.0), (8.0, 7.0)],
        5,
        &RED,
        &|c, s, st| {
            return EmptyElement::at(c)    // We want to construct a composed element on-the-fly
                + Circle::new((0, 0), s, st.filled()) // At this point, the new pixel coordinate is established
                + Text::new(format!("{:?}", c), (10, 0), ("monospace", 10).into_font());
        },
    ))?;

    Ok(())
}

fn main() {
    let matches = App::new("Diamond Reflection Remover for High-Pressure DAC Crystallography")
        .version("0.1.0")
        .author("BiChemicalBromance")
        .about("Extracts reflections above a certain intensity from a .raw file")
        .arg(Arg::new("INPUT_FILE")
            .about("The location of the raw from which to remove reflections")
            .required(true)
            .index(1))
        .arg(Arg::new("CUTTOFF_INTENSITY")
            .about("The intensity value above which reflections will be discarded")
            .required(true)
            .index(2))
        .arg(Arg::new("v")
            .short('v')
            .takes_value(true)
            .about("Sets the level of verbosity"))
        .get_matches();


    let cutoff = match matches.value_of("CUTTOFF_INTENSITY").unwrap().parse::<f64>() {
        Ok(c) => { c }
        Err(_) => { panic!() }
    };

    let mut contents_buffer;
    {
        let mut raw_file_handle: File = match File::open(matches.value_of("INPUT_FILE").unwrap()) {
            Ok(file) => { file }
            Err(_) => {
                panic!()
            }
        };
        contents_buffer = String::new();
        raw_file_handle.read_to_string(&mut contents_buffer).expect("Could not read from file into string");
    }
    let mut lines: Vec<&str> = contents_buffer.split("\n").collect();
    let mut indices_to_delete: Vec<usize> = Vec::new();
    let space_replace_regex: Regex = Regex::new(r"\s+").unwrap();
    for (idx, line) in lines.iter().enumerate() {
        let csv_line = space_replace_regex.replace_all(line, ",").to_string();
        let csv_columns: Vec<&str> = csv_line.split(",").collect();
        if csv_columns.len() > 1 && csv_columns[4].parse::<f64>().unwrap() > cutoff {
            indices_to_delete.push(idx);
            println!("Found value {} at line index {} (hkl={},{},{})", csv_columns[4].parse::<f64>().unwrap(), idx,
                     csv_columns[1].parse::<i64>().unwrap(), csv_columns[2].parse::<i64>().unwrap(), csv_columns[3].parse::<i64>().unwrap())
        }
    }

    for (idx, line_index) in indices_to_delete.into_iter().enumerate() {
        lines.remove((line_index) - idx);
    }

    let lines_joined = lines.join("\n");
    match std::fs::rename(matches.value_of("INPUT_FILE").unwrap(), format!("{}_backup", matches.value_of("INPUT_FILE").unwrap())) {
        Ok(_) => { () }
        Err(_) => { panic!() }
    }
    let mut raw_file_handle_output = File::create(matches.value_of("INPUT_FILE").unwrap()).expect("Could not write output file");
    raw_file_handle_output.write_all(lines_joined.as_bytes()).expect("Could not write to file");
    plot();
}
